#pragma once

#include "nav_msgs/Odometry.h"
#include "ros/node_handle.h"
#include <ros/ros.h>

using namespace std;
// Declaration: .h Definition: .cpp

struct Robot_position
{
  float x;
  float y;
  float z;
};

class OdomClass 
{
  private:
    ros::NodeHandle* _nh;
    ros::Subscriber _sub_odom;
    Robot_position pose;

    void odomCallback(
      const nav_msgs::OdometryConstPtr &odom_msg);
    

  public:
    // constructor
    OdomClass(ros::NodeHandle &nodeHandle);

    
    Robot_position getPosition();
};


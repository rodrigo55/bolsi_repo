#include "exercise31_pkg/exercise31.h"
#include "ros/node_handle.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "odom_class_node");
  ros::NodeHandle nodeHandle("~");

  OdomClass odomClass(nodeHandle);

  Robot_position pose = odomClass.getPosition();
  
  ros::spin();

  return 0;
}
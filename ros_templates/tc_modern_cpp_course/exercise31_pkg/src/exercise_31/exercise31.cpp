#include "exercise31_pkg/exercise31.h"
#include "nav_msgs/Odometry.h"
#include "ros/node_handle.h"
#include "ros/subscriber.h"

// Constructor
OdomClass::OdomClass(ros::NodeHandle& nodeHandle) {
  this ->_nh = &nodeHandle;
  this ->_sub_odom = this->_nh->subscribe("/robot1/odom", 10, &OdomClass::odomCallback, this);
}

// Methods 
void OdomClass::odomCallback(
  const nav_msgs::OdometryConstPtr &odom_msg) {
  this->pose.x = odom_msg->pose.pose.position.x;
  this->pose.y = odom_msg->pose.pose.position.y;
  this->pose.z = odom_msg->pose.pose.position.z;
  ROS_INFO("Pose: [%.2f, %.2f, %.2f]", pose.x, pose.y, pose.z);
  }

Robot_position OdomClass::getPosition(){
        return this->pose;
}
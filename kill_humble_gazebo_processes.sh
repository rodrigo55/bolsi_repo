#!/bin/bash

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit
fi

# Find and kill processes containing the specified keywords
processes=$(pgrep -a -f 'humble|gazebo|gzserver')
if [ -n "$processes" ]; then
    echo "Processes found, killing..."
    echo "$processes" | while read -r line; do
        kill -9 $(echo "$line" | awk '{print $1}')
    done
    echo "Processes killed."
else
    echo "No processes found."
fi
